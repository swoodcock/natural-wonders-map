import sqlite3
import re
import os
import socket
import webbrowser

from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.textinput import TextInput
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.bubble import Bubble
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.scrollview import ScrollView
from kivy.uix.recycleview import RecycleView
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.modalview import ModalView
from kivy.uix.scatter import Scatter

from kivy.uix.image import Image, AsyncImage
from kivy.uix.behaviors import ButtonBehavior
from kivy.properties import StringProperty, BooleanProperty, NumericProperty, ObjectProperty

from kivy.uix.screenmanager import ScreenManager, Screen, NoTransition
from kivy.core.window import Window
from kivy.utils import platform

from kivy.uix.settings import SettingsWithNoMenu
from settingsjson import app_json

from kivy.garden.mapview import MapView, MapMarker, MapLayer
from kivy.garden.mapview.mapview.clustered_marker_layer import ClusteredMarkerLayer, ClusterMapMarker
#from kivy.garden.mapview.mapview.utils import haversine, get_zoom_for_radius

from kivy.clock import Clock, mainthread

from kivy.lang import Builder

from plyer import email, gps

#import sys
#reload(sys)
#sys.setdefaultencoding('utf8')

##########################################
############ INITIALISATION  #############
##########################################

class SM(ScreenManager): transition=NoTransition()
class LowerInterface(BoxLayout): pass
class UpperInterface(BoxLayout): defaultTitle = StringProperty('Country Select')
class ImageButton(ButtonBehavior, Image): pass
class BackButton(ButtonBehavior, Image): pass
class BackButtonDataScr(ButtonBehavior, Image): pass       
class DataGrid(GridLayout): pass
class CountryButton(Button): countryName = StringProperty('')
class SearchCountryBtn(Button): countryName = StringProperty(''); countryWonders = StringProperty('')
class SearchNameBtn(Button):
    wonderName = StringProperty('')
    wonderType = StringProperty('')
    wonderCountry = BooleanProperty()
class NameButton(Button):
    wonderName = StringProperty('')
    typeIcon = StringProperty('')
    onBorder = BooleanProperty()
class MarkerPopup(Bubble): popupTitle = StringProperty()
class TitleLabel(Label): pass
class MultilineLabel(Label): pass
class ColouredScrollView(ScrollView): pass    
class NoResults(BoxLayout): pass
class FilterButton(ButtonBehavior, Image): pass
class GPSButton(ButtonBehavior, Image): pass
class ExitPopup(Popup): pass
class DataScrLayout(GridLayout): pass
class DataScrSpacer(BoxLayout): pass
class ImagePlaceholderBox(BoxLayout): imageSource = StringProperty()
class AsyncPopup(ModalView): imageUrl = StringProperty()
class ZoomableImage(Scatter): imageSource = StringProperty()
class TierTypeBox(BoxLayout): wonderTier = StringProperty(); wonderType = StringProperty()
class DataScrBox(BoxLayout): dataScrTitle = StringProperty(); dataScrText = StringProperty()
class LocationBox(BoxLayout): dataScrLoc = StringProperty()
class TierBubble(Bubble): pass
class TypeBubble(Bubble): pass
class SearchWebBtn(Button): pass
### Temp until nested scrollviews are fixed
class DataScrollView(ScrollView):
    pass
#    def on_touch_up(self, touch):
#        if self.collide_point(*touch.pos):
#            App.get_running_app().root.get_screen('data').scrollLayout.do_scroll_y = False
#            return True
#        else:
#            return super(DataScrollView, self).on_touch_down(touch)
#    def on_touch_down(self, touch):
#        if self.collide_point(*touch.pos):
#            App.get_running_app().root.get_screen('data').scrollLayout.do_scroll_y = False
#            return True
#        else:
#            App.get_running_app().root.get_screen('data').scrollLayout.do_scroll_y = True
#            return super(DataScrollView, self).on_touch_down(touch)
###########################################  

##############################
############ MAP #############
##############################

class MapScreen(Screen):
    def __init__(self, **kwargs):
        super(MapScreen, self).__init__(**kwargs)
        self.natuMap = CustMapView(zoom=3, lat=50.6394, lon=3.057)
        self.natuMap.map_source = 'osm'
        self.natuMap.map_source.min_zoom = 2
        self.clustLayer = ClusteredMarkerLayer(cluster_cls=ClustMark, cluster_max_zoom=10, cluster_extent=400)
        #cluster_extent defaults to 512, cluster_radius is relative to it. Note: change max_zoom on datascr also
        self.natuMap.add_layer(self.clustLayer)
        self.popupLayer = MapLayer()
        self.pushedMapButton = False
        self.natuMap.add_layer(self.popupLayer)
        self.add_widget(self.natuMap)
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute("SELECT lat, lon FROM naturalwonders")
        dbLatLon = [r for r in cur.fetchall()]
        cur.close()
        conn.close()
        for i in xrange(len(dbLatLon)):
            dbLat = dbLatLon[i][0]
            dbLon = dbLatLon[i][1]
            if not dbLat == '' or not dbLon == '':
                self.clustLayer.add_marker(lat=float(dbLat), lon=float(dbLon), cls=PopMark)

        self.dropDown = DropDown()
        pngIndex = ['no_filter.png', 'new7w.png', 'major.png', 'minor.png', 'wildlife.png', 'volcano.png', 'summit.png', 'beach.png']
        pngIndex = ['icons/' + name for name in pngIndex]
        self.dropDown.add_widget(Label(size_hint_y=None, height='10dp'))
        for i in xrange(len(pngIndex)):
            #btn = FilterButton(source=pngIndex[i], size_hint_y=None, height='40dp')
            btn = FilterButton(source=pngIndex[i], size_hint_y=None, height=Window.height*.075)
            btn.bind(on_release=lambda btn: self.dropDown.select(btn.source))
            self.dropDown.add_widget(btn)
            self.dropDown.add_widget(Label(size_hint_y=None, height='7dp'))
        self.mainButton = ImageButton(source='icons/filter.png')
        self.mainButton.bind(on_release=self.dropDown.open)
        self.dropDown.bind(on_select=lambda instance, x: setattr(self.mainButton, 'source', x))
        self.filterLayout = BoxLayout(size_hint = (.15, .075), pos_hint = {'center_x': .1, 'center_y': .95})
        self.filterLayout.add_widget(self.mainButton)
        self.add_widget(self.filterLayout)

    def reload_map(self, filterSelect):
        curLat = self.natuMap.lat
        curLon = self.natuMap.lon
        self.natuMap.remove_layer(self.clustLayer)
        self.natuMap.remove_layer(self.popupLayer)
        self.clustLayer = ClusteredMarkerLayer(cluster_cls=ClustMark, cluster_max_zoom=10, cluster_extent=350)
        self.natuMap.add_layer(self.clustLayer)
        self.popupLayer = MapLayer()
        self.markerPopup = Bubble(orientation='vertical', size_hint=(None, None), size=('130dp','100dp'))
        self.pushedMapButton = False
        self.natuMap.add_layer(self.popupLayer)
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()
        if filterSelect == 'icons/no_filter.png':
            self.filterLayout.remove_widget(self.mainButton)
            self.mainButton = ImageButton(source='icons/filter.png')
            self.mainButton.bind(on_release=self.dropDown.open)
            self.filterLayout.add_widget(self.mainButton)
            cur.execute("SELECT lat, lon FROM naturalwonders")
        elif filterSelect == 'icons/new7w.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE tier = '1'")            
        elif filterSelect == 'icons/major.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE tier = '2'")
        elif filterSelect == 'icons/minor.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE tier = '3'")
        elif filterSelect == 'icons/wildlife.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE wild = 'y'")
        elif filterSelect == 'icons/volcano.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE vol = 'y'")
        elif filterSelect == 'icons/summit.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE type = 'Summit'")
        elif filterSelect == 'icons/beach.png':
            cur.execute("SELECT lat, lon FROM naturalwonders WHERE type = 'Beach'")
        dbLatLon = [r for r in cur.fetchall()]
        cur.close()
        conn.close()
        for i in xrange(len(dbLatLon)):
            dbLat = dbLatLon[i][0]
            dbLon = dbLatLon[i][1]
            if dbLat == '' or dbLon == '':
                pass
            else:
                self.clustLayer.add_marker(lat=float(dbLat), lon=float(dbLon), cls=PopMark)
        self.natuMap.center_on(curLat, curLon)

    def center_screen(self, markLat, markLon):
        self.natuMap.center_on(float(markLat), float(markLon))

    def add_gps_button(self):
        self.gpsLayout = FloatLayout(size_hint=(.15, .075), pos_hint={'center_x': .9, 'center_y': .3})
        gpsBtn = GPSButton()
        self.gpsLayout.add_widget(gpsBtn)
        self.add_widget(self.gpsLayout)

    def remove_gps_button(self):
        self.remove_widget(self.gpsLayout)

    def load_popup_content(self, markLat, markLon):
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute("SELECT name FROM naturalwonders WHERE lat = ? AND lon = ?", (str(markLat), str(markLon)))
        dbQueryName = cur.fetchone()
        cur.close()
        conn.close()
        if dbQueryName == None:
            errorContent = BoxLayout(orientation='vertical')
            errorContent.add_widget(MultilineLabel(text='No Data Entry Found For Coordinates!', color=[1,1,1,1], halign='center'))
            dismissBtn = Button(text='OK', size_hint=(.4,.35), pos_hint={'center_x':.5})
            errorContent.add_widget(dismissBtn)
            errorMsg = Popup(title='Incorrect Coordinates', content=errorContent, size_hint=(.5,.3), title_align='center')
            dismissBtn.bind(on_press=errorMsg.dismiss)
            errorMsg.open()
        else:
            App.get_running_app().root.get_screen('data').wonderName = dbQueryName[0]
            #self.markerPopup.clear_widgets()
            #self.markerPopup.add_widget(TitleLabel(text=dbQueryName[0], size_hint_y=.7, halign='center', color=[1,1,1,1], padding_x='3dp'))
            #nameBtn = MapNameBtn(text='View Info', size_hint_y=.3, background_color=[.58, .78, .84, .8])
            #self.markerPopup.add_widget(nameBtn)
            self.markerPopup = MarkerPopup(popupTitle=dbQueryName[0])
            markerSource = App.get_running_app().markerIcon
            if markerSource == 'Default Source': self.markerPopup.pos = Window.width/2-self.markerPopup.width/2, Window.height/2+(self.markerPopup.height/35)*50
            elif markerSource == 'icons/map_marker_pin.png': self.markerPopup.pos = Window.width/2-self.markerPopup.width/2, Window.height/2+(self.markerPopup.height/35)*19
            else: self.markerPopup.pos = Window.width/2-self.markerPopup.width/2, Window.height/2+self.markerPopup.height
            self.popupLayer.add_widget(self.markerPopup)
       
class CustMapView(MapView):
#    def __init__(self, **kwargs):
#        super(CustMapView, self).__init__(**kwargs)
    
    def on_map_relocated(self, *kwargs):
        pass
#        x1, y1, x2, y2 = self.get_bbox()
#        centerX, centerY = Window.center
#        #centerLat, centerLon = self.get_latlon_at(centerX, centerY, zoom=self.zoom)
#        #latRemainder = centerLat-(x1+x2)/2
#        latRemainder = self.get_latlon_at(centerX, centerY, zoom=self.zoom)[0]-(x1+x2)/2
#        if x1 < -85.8: self.center_on((x1+x2)/2+latRemainder+.2, self.lon)
#        if x2 > 83.6: self.center_on((x1+x2)/2+latRemainder-.2, self.lon)
#        #if int(x1)==-86: self.center_on((x1+x2)/2+latRemainder+.2, self.lon)
#        #if int(x2)==84: self.center_on((x1+x2)/2+latRemainder-.2, self.lon)
#        if y1 == -180: self.center_on(self.lat, (y1+y2)/2+0.01)
#        if y2 == 180: self.center_on(self.lat, (y1+y2)/2-0.01)

class PopMark(MapMarker):
    isOpen = False
    def __init__(self, **kwargs):
        super(PopMark, self).__init__(**kwargs)
        markerSource = App.get_running_app().markerIcon
        if markerSource == 'Default Source':
            self.source = self.source
        else:
            self.source = markerSource
            self.size = (self.width/2, self.height/2)
        self.mapScr = App.get_running_app().root.get_screen('map')

    def on_touch_down(self, touch):
        if self.collide_point(*touch.pos):
            if self.mapScr.pushedMapButton == True:
                self.isOpen = True
            else:
                self.isOpen = not self.isOpen
        else:
            self.isOpen = False
        self.mapScr.popupLayer.clear_widgets()
        return super(PopMark, self).on_touch_down(touch)

    def on_release(self):
        if self.isOpen == True:
            self.mapScr.center_screen(self.lat, self.lon)
            self.mapScr.load_popup_content(self.lat, self.lon)
            self.mapScr.pushedMapButton = False

class ClustMark(ClusterMapMarker):
    liftUp = True
    def __init__(self, **kwargs):
        super(ClustMark, self).__init__(**kwargs)
        self.source = 'icons/cluster_marker.png'
        
    def on_touch_up(self, touch):
        if self.collide_point(*touch.pos):
            if self.liftUp == True:
                App.get_running_app().root.get_screen('map').center_screen(self.lat, self.lon)
                Clock.schedule_once(self.update_map_zoom, .01)
            self.liftUp = True
        else:
            self.liftUp = True
            return super(ClustMark, self).on_touch_down(touch)

    def on_touch_move(self, touch):
        self.liftUp = False

    def update_map_zoom(self, *args):
        mapScr = App.get_running_app().root.get_screen('map')
        updateZoom = mapScr.natuMap.zoom + 1
        if updateZoom > mapScr.natuMap.map_source.get_max_zoom(): pass
        else: mapScr.natuMap.zoom = updateZoom

#######################################
############ Data Display #############
#######################################

class SearchBox(TextInput):
    def on_text(self, *kwargs):
        App.get_running_app().root.get_screen('search').load_data(self.text)

class SearchScreen(Screen):
    eventScheduled = False
    def __init__(self, **kwargs):
        super(SearchScreen, self).__init__(**kwargs)
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()           
        cur.execute("SELECT name, type, country, tier FROM naturalwonders")
        self.wonderInfo = [r for r in cur.fetchall()]
        cur.close()
        conn.close()
        self.wonderInfo.sort()
#        for i in reversed(xrange(len(dbCountry))):
#            if ';' in dbCountry[i]:
#                countrySplit = dbCountry[i].split(';')
#                dbCountry.remove(dbCountry[i])
#                for country in reversed(xrange(len(countrySplit))):                       
#                    while countrySplit[country] not in dbCountry:
#                        dbCountry.append(countrySplit[country])
#        dbCountry.sort()

        #self.dataView = ColouredRecycleView(size_hint_y=.82, pos_hint={'center_y':.49})
        self.dataView = ColouredRecycleView(size_hint_y=.9)
        self.removeText = BoxLayout(size_hint=(.15,.1), pos_hint={'center_x':.925,'center_y':.95})
        self.loadingBox = BoxLayout(size_hint=(.15,.1), pos_hint={'center_x':.775,'center_y':.95})
        self.loadingImage = Image(source='icons/spinner.zip', size_hint=(.7,.7), pos_hint={'center_x':.5,'center_y':.5}, anim_delay=0, keep_data=True)
        self.noResults = NoResults()
        searchBox = BoxLayout(size_hint=(.5,.1), pos_hint={'center_x':.45,'center_y':.95})
        self.inputText = SearchBox(text='')
        searchBox.add_widget(self.inputText)
        self.add_widget(self.dataView)
        self.add_widget(self.removeText)
        self.add_widget(self.loadingBox)
        self.add_widget(searchBox)

    def load_data(self, searchStr):
        if searchStr == '':
            self.dataView.clear_widgets()
            self.removeText.clear_widgets()
            self.loadingBox.clear_widgets()
            self.dataView.add_widget(self.noResults)
            if self.eventScheduled == True:
                self.displayEvent.cancel()
                self.eventScheduled = False
        else:
            self.loadingBox.clear_widgets()           
            self.loadingBox.add_widget(self.loadingImage)
            if self.eventScheduled == True:
                self.displayEvent.cancel()
            self.displayEvent = Clock.create_trigger(lambda x: self.display_search(searchStr), .6)
            self.displayEvent()
            self.eventScheduled = True

    def display_search(self, searchStr):
        self.dataView.clear_widgets()
        self.removeText.clear_widgets()
        self.loadingBox.clear_widgets()
        self.eventScheduled = False
        
        altCountries = [line.rstrip() for line in open('alt_country_names.txt')]
        altCountries  = [x.split(':') for x in altCountries]
        cList = [x[0] for x in altCountries]
        altList = [x[1].split(',') for x in altCountries]
        searchUpper = searchStr.title()
        for i in xrange(len(altList)):
            if searchUpper in altList[i] or searchUpper.upper() in altList[i]:
                searchStr = cList[i]
        #searchUpper = searchStr[0].upper() + searchStr[1:]
        #rePattern = re.compile('^.*' + searchUpper + '.*$')
        rePattern = re.compile('^.*\\b' + re.escape(searchStr) + '.*$', re.IGNORECASE)

        dGrid = DataGrid()
        countryMatch = [x for x in self.countryList if rePattern.match(x)]
        countryMatch.sort(key=lambda x:(not x[0].startswith(searchStr[0])))
        for i in xrange(len(countryMatch)):
            official = 0; major = 0; minor = 0
            matchedWonder = [x for x in self.wonderInfo if re.match('^.*' + countryMatch[i] + '.*$', x[2])]
            for wonder in matchedWonder:
                if wonder[3] == '1':
                    official = official + 1
                if wonder[3] == '2':
                    major = major + 1
                if wonder[3] == '3':
                    minor = minor + 1
            tierStr = str(official+major+minor) + ' Wonders: ' + str(official) + ' Official, ' + str(major) + ' Major, ' + str(minor) + ' Minor'
            countryWidget = SearchCountryBtn(countryName=str(countryMatch[i]), countryWonders=tierStr)
            dGrid.add_widget(countryWidget)

        nameMatch = [x for x in self.wonderInfo if rePattern.match(x[0])]
        nameMatch.sort(key=lambda x:(not x[0].startswith(searchStr[0].upper())))
        wonderName = [x[0] for x in nameMatch]
        wonderType = [x[1] for x in nameMatch]
        matchedCountry = [x[2] for x in nameMatch]
        for i in xrange(len(nameMatch)):
            if ';' in matchedCountry[i]:
                countrySplit = matchedCountry[i].split(';')
                wonderCountry = 'Border: ' + ', '.join(countrySplit)
            else:
                wonderCountry = matchedCountry[i]
            nameWidget = SearchNameBtn(wonderName=str(wonderName[i]), wonderType=wonderType[i], wonderCountry=wonderCountry)
            dGrid.add_widget(nameWidget)

        crossImage  = ImageButton(size_hint=(.25, .25), source='icons/clear.png', pos_hint={'center_y': .5})
        crossImage.bind(on_release=self.clear_text)
        self.removeText.add_widget(crossImage)

        if not countryMatch and not nameMatch:
            self.dataView.add_widget(self.noResults)
        else:
            self.dataView.add_widget(dGrid)

    def clear_text(self, *args):
        self.inputText.text = ''

class CountryListScreen(Screen):
    def __init__(self, **kwargs):
        super(CountryListScreen, self).__init__(**kwargs)
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()           
        cur.execute("SELECT distinct country FROM naturalwonders")
        dbCountry = [r[0] for r in cur.fetchall()]
        cur.close()
        conn.close()
        for i in reversed(xrange(len(dbCountry))):
            if ';' in dbCountry[i]:
                countrySplit = dbCountry[i].split(';')
                dbCountry.remove(dbCountry[i])
                for country in reversed(xrange(len(countrySplit))):                       
                    while countrySplit[country] not in dbCountry:
                        dbCountry.append(countrySplit[country])
        dbCountry.sort()
        scrollLayout = ColouredScrollView(size_hint_y=.82, pos_hint={'center_y':.49})
        countryGrid = DataGrid()
        for i in xrange(len(dbCountry)):
            countryButton = CountryButton(countryName=str(dbCountry[i]))
            countryGrid.add_widget(countryButton)
        scrollLayout.add_widget(countryGrid)
        self.add_widget(scrollLayout)
        self.countryList = dbCountry

class ColouredRecycleView(RecycleView):
    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            return True
        else:
            App.get_running_app().root.get_screen('search').inputText.focus = False
            return super(ColouredRecycleView, self).on_touch_down(touch)
        
class CountryScreen(Screen):
    listToCountry = False
    searchToCountry = False
    def __init__(self, **kwargs):
        super(CountryScreen, self).__init__(**kwargs)
        self.scrollLayout = ColouredScrollView(size_hint_y=.82, pos_hint={'center_y':.49})
        self.add_widget(self.scrollLayout)

    def on_pre_enter(self):
        searchStr = App.get_running_app().root.get_screen('country').textStr
        self.topTitle = UpperInterface(defaultTitle = str(searchStr))
        self.add_widget(self.topTitle)
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute("SELECT name, type, country FROM naturalwonders WHERE country LIKE ?", ('%'+searchStr+'%',))
        dbQuery = [r for r in cur.fetchall()]
        dbQuery.sort()
        allWonders = [elem[0] for elem in dbQuery]
        wonderType = [elem[1] for elem in dbQuery]
        wonderCountry = [elem[2] for elem in dbQuery]
        cur.close()
        conn.close()

        dataLayout = DataGrid()
        for i in xrange(len(allWonders)):
            if wonderType[i] == 'Phenomena':
                typePng = 'icons/phenomena.png'
            elif wonderType[i] == 'Region':
                typePng = 'icons/region.png'
            elif wonderType[i] == 'Summit':
                typePng = 'icons/summit.png'
            elif wonderType[i] == 'Beach':
                typePng = 'icons/beach.png'
            elif wonderType[i] == 'Trailhead':
                typePng = 'icons/trailhead.png'
            if ';' in wonderCountry[i]:
                borderWonder = True
            else:
                borderWonder = False
            value = NameButton(wonderName=str(allWonders[i]), typeIcon=typePng, onBorder=borderWonder)
            dataLayout.add_widget(value)
        self.scrollLayout.add_widget(dataLayout)

    def on_leave(self):
        self.scrollLayout.clear_widgets()
        self.remove_widget(self.topTitle)

class DataScreen(Screen):
    countryToData = False
    searchToData = False
    def __init__(self, **kwargs):
        super(DataScreen, self).__init__(**kwargs)
        self.scrollLayout = ColouredScrollView(size_hint_y=.82, pos_hint={'center_y':.49})
        self.add_widget(self.scrollLayout)

    def on_pre_enter(self):
        app = App.get_running_app()
        searchStr = app.root.get_screen('data').wonderName
        conn = sqlite3.connect('naturalwonders.sqlite3')
        conn.text_factory = str
        cur = conn.cursor()
        cur.execute("SELECT name, tier, type, country, lat, lon, elev, desc, note, image FROM naturalwonders WHERE name LIKE ?", (searchStr,))
        wonderInfo = [r for r in cur.fetchone()]
        cur.close()
        conn.close()

        if wonderInfo[1] == '1': tier = '[1] World'
        elif wonderInfo[1] == '2': tier = '[2] National'
        elif wonderInfo[1] == '3': tier = '[2] Local'
        else: tier = 'Unknown'
        dataLayout = DataScrLayout()
        dataLayout.add_widget(TierTypeBox(wonderTier=tier, wonderType=wonderInfo[2]))
        dataLayout.add_widget(DataScrSpacer())
        
        if ';' in wonderInfo[3]: countrySplit = wonderInfo[3].split(';'); allCountries = 'Border: ' + ', '.join(countrySplit)
        else: allCountries = wonderInfo[3]
        dataLayout.add_widget(DataScrBox(dataScrTitle='Country', dataScrText=allCountries))
        dataLayout.add_widget(DataScrSpacer())
        
        tempDataBox = LocationBox(dataScrLoc=wonderInfo[4]+'\n'+wonderInfo[5])
        mapButton = ImageButton(source='icons/gotomap.png', size_hint=(.2,None), height='40dp', pos_hint={'center_x': 0.90, 'center_y': 0.5})
        mapButton.bind(on_release=self.go_to_map)
        tempDataBox.add_widget(mapButton)
        dataLayout.add_widget(tempDataBox)
        dataLayout.add_widget(DataScrSpacer())
        
        if wonderInfo[2] == 'Summit':
            dataLayout.add_widget(DataScrBox(dataScrTitle='Elevation', dataScrText=wonderInfo[6] + ' m'))
            dataLayout.add_widget(DataScrSpacer())

        dataLayout.add_widget(BottomDataBox(scrollTitle='Description', scrollText=wonderInfo[7], buttonText='', buttonImage=''))
        dataLayout.add_widget(DataScrSpacer())
        if wonderInfo[8] == '': wonderNotes = 'There are currently no notes for this wonder.'
        else: wonderNotes = wonderInfo[8]
        dataLayout.add_widget(BottomDataBox(scrollTitle='Notes', scrollText=wonderNotes, buttonText='Add notes', buttonImage='icons/map_plus.png'))
        #dataLayout.add_widget(DataScrSpacer())

        searchBtn = SearchWebBtn()
        searchBtn.bind(on_release=lambda x: webbrowser.open("http://www.google.com/search?ie=UTF-8&q="+searchStr))
        dataLayout.add_widget(searchBtn)

        imageFile = 'images/' + searchStr.replace(" ", "_").lower() + '.jpg'
        if not os.path.isfile(imageFile): imageFile = 'icons/image_placeholder.png'
        imageHolder = ImagePlaceholderBox(imageSource=imageFile)
        imageHolder.add_widget(dataLayout)
        self.topTitle = UpperInterface(defaultTitle = str(searchStr))
        self.add_widget(self.topTitle)
        self.scrollLayout.add_widget(imageHolder)

        if wonderInfo[9] == '': self.imageUrl = 'icons/image_placeholder.png'
        else: self.imageUrl = wonderInfo[9]
        
        self.wonderLat = wonderInfo[4]
        self.wonderLon = wonderInfo[5]

    def on_leave(self):
        self.scrollLayout.clear_widgets()
        self.scrollLayout.scroll_y = 1
        self.remove_widget(self.topTitle)

    def go_to_map(self, *args):
        mapScr = App.get_running_app().root.get_screen('map')
        try:
            mapScr.reload_map('icons/no_filter.png')
            mapScr.natuMap.center_on(float(self.wonderLat), float(self.wonderLon))
            mapScr.natuMap.zoom = 11 #cluster_max_zoom = 10
            mapScr.load_popup_content(float(self.wonderLat), float(self.wonderLon))
            App.get_running_app().root.current = 'map'
        except:
            import traceback
            traceback.print_exc()
            errorContent = BoxLayout(orientation='vertical')
            errorContent.add_widget(MultilineLabel(text='No Coordinates Found For Data Entry!', color=[1,1,1,1], halign='center'))
            dismissBtn = Button(text='OK', size_hint=(.4,.35), pos_hint={'center_x':.5})
            errorContent.add_widget(dismissBtn)
            errorMsg = Popup(title='Incorrect Coordinates', content=errorContent, size_hint=(.5,.3), title_align='center')
            dismissBtn.bind(on_press=errorMsg.dismiss)
            errorMsg.open()

    def image_popup(self):
        if self.query_internet() == True:
            imagePopup = AsyncPopup(imageUrl=self.imageUrl)
            imagePopup.open()
        else:
            errorContent = BoxLayout(orientation='vertical')
            errorContent.add_widget(MultilineLabel(text='Please connect to the internet to view high resolution images.', color=[1,1,1,1], halign='center'))
            dismissBtn = Button(text='OK', size_hint=(.4,.35), pos_hint={'center_x':.5})
            errorContent.add_widget(dismissBtn)
            errorMsg = Popup(title='No Internet Connection', content=errorContent, size_hint=(.5,.3), title_align='center')
            dismissBtn.bind(on_press=errorMsg.dismiss)
            errorMsg.open()

    def query_internet(self):
        try:
            socket.create_connection(("www.google.com", 80))
            return True
        except: pass
        return False

class BottomDataBox(BoxLayout):
    scrollTitle = StringProperty()
    scrollText = StringProperty()
    buttonText = StringProperty()
    buttonImage = StringProperty()

    def send_email(self, *args):
        try:
            wonderName = App.get_running_app().root.get_screen('data').wonderName
            subjectStr = 'NoteRequest: ' + wonderName
            textStr = """Please help improve the app by sending us tips for other natural wonder visitors.
If you have some useful insight into accessing/better experiencing any wonder, it will be included in the notes section of our next app release. Thank you for your contribution!

####################
Add your note below:
####################

"""
            email.send(recipient='naturalwondersapp@gmail.com', subject=subjectStr, text=textStr)
        except:
            import traceback
            traceback.print_exc()
            errorContent = BoxLayout(orientation='vertical')
            errorContent.add_widget(MultilineLabel(text='No Email Client Found!', color=[1,1,1,1], halign='center'))
            dismissBtn = Button(text='OK', size_hint=(.4,.35), pos_hint={'center_x':.5})
            errorContent.add_widget(dismissBtn)
            errorMsg = Popup(title='Error Sending Email', content=errorContent, size_hint=(.5,.3), title_align='center')
            dismissBtn.bind(on_press=errorMsg.dismiss)
            errorMsg.open()

class TierInfo(ButtonBehavior, Image):
    bubblePresent = False
    def show_bubble_key(self, obj, *args):
        if self.bubblePresent == False:
            self.bubble = TierBubble()
            self.bubble.pos = obj.pos[0]-self.bubble.width/6+obj.width/2, obj.pos[1]+obj.height
            self.add_widget(self.bubble)
            self.bubblePresent = True
        else:
            self.remove_widget(self.bubble)
            self.bubblePresent = False

    def on_touch_down(self, touch):           
        if not self.collide_point(*touch.pos):
            if self.bubblePresent == True:
                self.remove_widget(self.bubble)
                self.bubblePresent = False
                return True
        else:
            return super(TierInfo, self).on_touch_down(touch)

class TypeInfo(ButtonBehavior, Image):
    bubblePresent = False
    def show_bubble_key(self, obj, *args):
        if self.bubblePresent == False:
            self.bubble = TypeBubble()
            self.bubble.pos = obj.pos[0]-self.bubble.width/6*5+obj.width/2, obj.pos[1]+obj.height
            self.add_widget(self.bubble)
            self.bubblePresent = True
        else:
            self.remove_widget(self.bubble)
            self.bubblePresent = False

    def on_touch_down(self, touch):
        if not self.collide_point(*touch.pos):
            if self.bubblePresent == True:
                self.remove_widget(self.bubble)
                self.bubblePresent = False
                return True
        else:
            return super(TypeInfo, self).on_touch_down(touch)

class WonderSettings(SettingsWithNoMenu):
    def __init__(self, **kwargs):
        super(WonderSettings, self).__init__(**kwargs)
        self.orientation = 'vertical'
        closeButton = Button(text='Close', size_hint=(None, None), size=('140dp', '60dp'), pos_hint={'center_x': .5})
        closeButton.bind(on_release=lambda x: App.get_running_app().close_settings())
        self.add_widget(closeButton)
        
##################################
############ APP RUN #############
##################################
class NaturalWondersApp(App):
    gps_location = StringProperty()
    gps_status = StringProperty()
    gpsConfigured = False
    gpsOn = False
    markerVisible = False
    gpsBtn = False
    appSuspend = False
    appLat = 0.0
    appLon = 0.0
    markerIcon = 'icons/map_marker.png'
  
    def build(self):
        #### Platform Specific Settings  ####
        if platform == 'android' or platform == 'ios':
            pass
        elif platform == 'linux' or platform == 'win' or platform == 'macosx':
            Window.size = (350, 615)
        elif platform == 'unknown':
            pass
        #####################################
        self.settings_cls = WonderSettings
        self.use_kivy_settings = False
        self.config   
        self.bind(on_start=self.post_build_init)

        #.ini file settings adjustments
        tempSet = self.config.get('app', 'locationPersist')
        if tempSet == '0': self.locationPersist = False
        elif tempSet == '1': self.locationPersist = True
        tempSet = self.config.get('app', 'gpsEnabled')
        if tempSet == '0': self.gpsEnabled = False
        elif tempSet == '1': self.gpsEnabled = True; self.gps_start(1000, 0)
        tempSet = self.config.get('app', 'markerIcon')
        if tempSet == 'Green (Default)': self.markerIcon = 'icons/map_marker.png'
        elif tempSet == 'Red (Large)': self.markerIcon = 'Default Source'
        elif tempSet == 'Blue (Narrow)': self.markerIcon = 'icons/map_marker_narrow.png'
        elif tempSet == 'Yellow Pin (Small)': self.markerIcon = 'icons/map_marker_pin.png'
        elif tempSet == 'Outline Only': self.markerIcon = 'icons/map_marker_outline.png'
        self.get_running_app().root.get_screen('map').reload_map('icons/no_filter.png')

    def build_config(self, config):
        config.setdefaults('app', {
        'gpsEnabled': '1',
        'locationPersist': '0',
        'markerIcon': 'Green Pins',
        'mapSource': 'OpenStreetMap'})

        config.setdefaults('offline', {
        'useOffline': '0',
        'optionsexample': 'option1',
        'tileLocation': '/some/path'})

    def build_settings(self, settings):
        settings.add_json_panel('Settings', self.config, data=app_json)

    def on_config_change(self, config, section, key, value):
        print config, section, key, value
        if key == 'gpsEnabled':
            if value == '0':
                self.gpsEnabled = False
                if self.gpsOn == True: self.gps_stop()
            elif value == '1':
                self.gpsEnabled = True
                if self.gpsOn == False: self.gps_start(1000, 0)
        if key == 'locationPersist':
            if value == '0':
                self.locationPersist = False
                if self.markerVisible == True:
                    mapScr = self.get_running_app().root.get_screen('map')
                    mapScr.natuMap.remove_marker(self.locationMark)
                    self.markerVisible = False
                    self.appLat = 0.0; self.appLon = 0.0
            elif value == '1':
                self.locationPersist = True
        if key == 'markerIcon':
            if value == 'Green (Default)': self.markerIcon = 'icons/map_marker.png'
            elif value == 'Red (Large)': self.markerIcon = 'Default Source'
            elif value == 'Blue (Narrow)': self.markerIcon = 'icons/map_marker_narrow.png'
            elif value == 'Yellow Pin (Small)': self.markerIcon = 'icons/map_marker_pin.png'
            elif value == 'Outline Only': self.markerIcon = 'icons/map_marker_outline.png'
            self.get_running_app().root.get_screen('map').reload_map('icons/no_filter.png')

    def on_pause(self):
        self.appSuspend = True
        if self.gpsOn == True:
            self.gps_stop()
            self.gpsOn = False
        App.get_running_app().root.get_screen('search').inputText.focus = False
        return True

    def on_resume(self):
        self.appSuspend = True
        if self.gpsEnabled == True:
            if self.gpsOn == False:
                self.gps_start(1000, 0)
                self.gpsOn = True
        self.appSuspend = False
        pass

## GPS Implementation Start    
    def gps_start(self, minTime, minDistance):
        try:
            if self.gpsConfigured == False:
                gps.configure(on_location=self.on_location, on_status=self.on_status)
                self.gpsConfigured = True
            gps.start(minTime, minDistance)
            self.gpsOn = True
            if self.appSuspend == False:
                if self.gpsBtn == False:
                    self.get_running_app().root.get_screen('map').add_gps_button()
                    self.gpsBtn = True
        except NotImplementedError:
            import traceback
            traceback.print_exc()
            self.gps_status = 'Please Activate GPS'

    def gps_stop(self):
        try:
            gps.stop()
            self.gpsOn = False
            if self.appSuspend == False:
                if self.markerVisible == True:
                    if self.locationPersist == False:
                        self.get_running_app().root.get_screen('map').natuMap.remove_marker(self.locationMark)
                        self.markerVisible = False
                        self.appLat = 0.0
                        self.appLon = 0.0
                if self.gpsBtn == True:
                    self.get_running_app().root.get_screen('map').remove_gps_button()
                    self.gpsBtn = False
        except NotImplementedError:
            import traceback
            traceback.print_exc()

    @mainthread
    def on_location(self, **kwargs):
        self.gps_location = '\n'.join(['{}={}'.format(k, v) for k, v in kwargs.items()])
        self.lat = kwargs.get('lat')
        self.lon = kwargs.get('lon')
        
        self.appLat = self.lat
        self.appLon = self.lon

        mapScr = self.get_running_app().root.get_screen('map')
        if self.markerVisible == True:
            mapScr.natuMap.remove_marker(self.locationMark)
        self.locationMark = MapMarker(lat=self.appLat, lon=self.appLon, source='icons/gps_loc_mark.png')
        self.locationMark.size = ('16dp', '16dp')
        mapScr.natuMap.add_marker(self.locationMark)
        self.markerVisible = True

    @mainthread
    def on_status(self, stype, status):
        self.gps_status = 'type={}\n{}'.format(stype, status) + 'random'
## GPS Implementation End

    def post_build_init(self,ev):
        from kivy.base import EventLoop
        EventLoop.window.bind(on_keyboard=self.hook_keyboard)

    def hook_keyboard(self, window, key, *largs):
        app = App.get_running_app()
        if key == 27:
            if app.open_settings() == False:
                app.close_settings()
                return True
            elif app.root.current == 'map':
                exitMsg = ExitPopup()
                exitMsg.open()
            else:
                if app.root.get_screen('data').searchToData == True: app.root.current = 'search'
                elif app.root.get_screen('country').searchToCountry == True: app.root.current = 'search'
                elif app.root.get_screen('data').countryToData == True:
                    app.root.get_screen('country').listToCountry = True
                    app.root.current = 'country'
                elif app.root.get_screen('country').listToCountry == True: app.root.current = 'countrylist'
                else: app.root.current = 'map'

if __name__ == "__main__":
    NaturalWondersApp().run()
